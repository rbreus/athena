#include "TrigT2CaloEgamma/T2CaloEgamma.h"
#include "TrigT2CaloEgamma/T2CaloEgammaFastAlgo.h"
#include "TrigT2CaloEgamma/T2CaloEgammaReFastAlgo.h"
#include "TrigT2CaloEgamma/EgammaSamp2Fex.h"
#include "TrigT2CaloEgamma/EgammaSamp1Fex.h"
#include "TrigT2CaloEgamma/EgammaEmEnFex.h"
#include "TrigT2CaloEgamma/EgammaHadEnFex.h"
#include "TrigT2CaloEgamma/EgammaReSamp2Fex.h"
#include "TrigT2CaloEgamma/EgammaReSamp1Fex.h"
#include "TrigT2CaloEgamma/EgammaReEmEnFex.h"
#include "TrigT2CaloEgamma/EgammaReHadEnFex.h"
#include "TrigT2CaloEgamma/RingerFex.h"
#include "TrigT2CaloEgamma/EgammaAllFex.h"

DECLARE_COMPONENT( T2CaloEgamma )
DECLARE_COMPONENT( T2CaloEgammaFastAlgo )
DECLARE_COMPONENT( T2CaloEgammaReFastAlgo )
DECLARE_COMPONENT( EgammaSamp2Fex )
DECLARE_COMPONENT( EgammaSamp1Fex )
DECLARE_COMPONENT( EgammaEmEnFex )
DECLARE_COMPONENT( EgammaHadEnFex )
DECLARE_COMPONENT( EgammaReSamp2Fex )
DECLARE_COMPONENT( EgammaReSamp1Fex )
DECLARE_COMPONENT( EgammaReEmEnFex )
DECLARE_COMPONENT( EgammaReHadEnFex )
DECLARE_COMPONENT( RingerFex )
DECLARE_COMPONENT( EgammaAllFex )
